INSERT INTO film (title, rental_rate, rental_duration)
VALUES ('Stardust', 4.99, 14);

INSERT INTO actor (first_name, last_name)
VALUES ('Charlie', 'Cox'),
       ('Claire', 'Danes'),
       ('Sienna', 'Miller');
       ('Mark', 'Strong');

INSERT INTO film_actor (actor_id, film_id)
VALUES (1,1), (2, 1), (3,1), (4,1);

INSERT INTO inventory (film_id, store_id)
VALUES (1, 1);
